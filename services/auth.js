const User = require("../model/User");
const ServerError = require("../utils/ServerError");
const bcrypt = require("bcryptjs");

async function checkEmailForRegister(userData) {
  const newUser = await User.findOne({ email: userData.email });

  if (newUser) {
    throw new ServerError(400, "Email already exists");
  }

  return userData;
}

async function registerUser(userData) {
  if (!userData) {
    throw new ServerError(400, "Somthing was wrong");
  }
  const newUser = new User({
    name: userData.name,
    email: userData.email,
    password: userData.password
  });
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      newUser.password = hash;
      newUser.save();
    });
  });

  return newUser;
}

async function checkEmailForLogin(userData) {
  const findUser = await User.findOne({ email: userData.email });

  if (!findUser) {
    throw new ServerError(400, "User not found");
  }

  return findUser;
}

module.exports = { checkEmailForRegister, registerUser, checkEmailForLogin };
