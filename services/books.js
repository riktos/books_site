const Book = require("../model/Book");
const ServerError = require("../utils/ServerError");

async function getBookById(id) {
  const book = await Book.findById(id);

  if (!book) {
    throw new ServerError(404, "Book not found");
  }

  return book;
}

async function deleteBookById(id) {
  const book = await Book.findByIdAndDelete(id);

  if (!book) {
    throw new ServerError(404, "Book not found");
  }

  return book;
}

async function addBook(bookData) {
  const book = new Book(bookData).save();

  if (!book) {
    throw new ServerError(400, "Somthing was wrong");
  }

  return book;
}

async function getBookList(searchParams, queryParams) {
  const books = await Book.find(searchParams, {}, queryParams);
  if (!books) {
    throw new ServerError(400, "Page not found");
  }

  return books;
}

async function getBooksCount(searchQuery) {
  const booksCount = await Book.count(searchQuery);
  return booksCount;
}

module.exports = {
  getBookById,
  deleteBookById,
  addBook,
  getBookList,
  getBooksCount
};
