const mongoose = require('mongoose'); // Erase if already required

// Declare the Schema of the Mongo model
const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        unique:true,
        index:true,
    },
    email:{
        type:String,
        required:true,
        unique:true,
        index:true,
    },
    date:{
        type: Date,
        default:Date.now
    },
    password:{
        type:String,
        required:true,
        index:true,
    },
});

//Export the model
module.exports = mongoose.model('User', userSchema);