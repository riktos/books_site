const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let Book = new Schema({
    title: {
        type: String,
        required:true
    },
    author:{
        type: String,
        required:true
    },
    desc: {
        type: String,
    },
    price: {
        type: String
    },
    url: {
        type: String
    }, 
    image: {
        type: String
    }
});

module.exports = mongoose.model("Book", Book)