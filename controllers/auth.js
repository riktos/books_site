const ServerError = require("../utils/ServerError");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");
const {
  checkEmailForRegister,
  checkEmailForLogin,
  registerUser
} = require("../services/auth");

async function register(req, res) {
  try {
    await checkEmailForRegister(req.body);
    await registerUser(req.body);
    res.status(200).json({ msg: "Book added was successful" });
  } catch (error) {
    if (error instanceof ServerError) {
      return res.status(error.statusCode).json({ error });
    }
    return res.status(500).json({ error: { message: "Email already exists" } });
  }
}

async function login(req, res) {
  try {
    const userData = await checkEmailForLogin(req.body);
    const { password } = req.body;
    bcrypt.compare(password, userData.password).then(isMatch => {
      if (isMatch) {
        const payload = {
          id: userData.id,
          name: userData.name
        };

        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926 // 1 year in seconds
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  } catch (error) {
    if (error instanceof ServerError) {
      return res.status(error.statusCode).json({ error });
    }
    return res.status(500).json({ error: { message: "Email notfound" } });
  }
}

module.exports = {
  register,
  login
};
