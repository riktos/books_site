const ServerError = require("../utils/ServerError");
const {
  getBookById,
  deleteBookById,
  addBook,
  getBookList,
  getBooksCount
} = require("../services/books");

async function getBook(req, res) {
  const { id } = req.params;

  if (!id) {
    return res.status(400).json({ error: { message: "Book is not valid!" } });
  }

  try {
    const book = await getBookById(id);

    res.status(200).json(book);
  } catch (error) {
    if (error instanceof ServerError) {
      return res.status(error.statusCode).json({ error });
    }

    return res
      .status(500)
      .json({ error: { message: "Something went wrong!" } });
  }
}

async function deleteBook(req, res) {
  const { id } = req.params;

  if (!id) {
    return res.status(400).json({ error: { message: "Book is not valid!" } });
  }

  try {
    await deleteBookById(id);

    res.status(200).json({ msg: "Book entry deleted successfully" });
  } catch (error) {
    if (error instanceof ServerError) {
      return res.status(error.statusCode).json({ error });
    }
    return res
      .status(500)
      .json({ error: { message: "Something went wrong!" } });
  }
}

async function newBook(req, res) {
  const bookData = req.body;

  if (!bookData) {
    return res
      .status(400)
      .json({ error: { message: "Somsing was wrong. Check all flields pls" } });
  }

  try {
    await addBook(bookData);
    res.status(200).json({ msg: "Book added was successful" });
  } catch (error) {
    if (error instanceof ServerError) {
      return res.status(error.statusCode).json({ error });
    }
    return res
      .status(500)
      .json({ error: { message: "Something went wrong!" } });
  }
}

async function getBooks(req, res) {
  const page = parseInt(req.query.page);
  const size = parseInt(req.query.size);
  const regexp = new RegExp(req.query.title, "igm");
  const query = {};

  if (page < 0 || page === 0 || typeof page != "number") {
    return res.json({ msg: "invalid page number, should start with 1" });
  }
  query.skip = size * (page - 1);
  query.limit = size;

  const searchQuery = { title: { $regex: regexp } };
  
  try {
    const data = await getBookList(searchQuery, query);
    const items = await getBooksCount(searchQuery);
    if (items == 0 || !data) {
      res.status(400).json({ msg: "Books not found" });
    }
    res.status(200).json({ books: data, items: items });
  } catch (error) {
    if (error instanceof ServerError) {
      return res.status(error.statusCode).json({ error });
    }
    return res
      .status(500)
      .json({ error: { message: "Something went wrong!" } });
  }
}

module.exports = { getBook, deleteBook, newBook, getBooks };
``