import createReducer from './reducers';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import api from './services';
// import jwt_decode from 'jwt-decode';
// import setAuthToken from './utils/setAuthToken';

let preState = {};
// if (localStorage.jwtToken) {
//   const token = localStorage.jwtToken;
//   setAuthToken(token);

//   preState = {auth: {login: {payload: jwt_decode(token)}}};
//   store.dispatch(setCurrentUser(decoded));

//   const currentTime = Date.now() / 1000;

//   if (decoded.exp < currentTime) {
//     store.dispatch(logoutUser());
//     window.location.href = "./login";
//   }
// }


export default function (history) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  return createStore(
    createReducer(history),
    preState,
    composeEnhancers(applyMiddleware(thunk.withExtraArgument(api))));
}
