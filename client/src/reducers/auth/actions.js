import jwt_decode from "jwt-decode";
import setAuthToken from "../../utils/setAuthToken";
import validateEmail from "../../utils/validate";
import {
  REGISTRATION,
  REGISTRATION_SUCCESS,
  REGISTRATION_ERROR,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR
} from "./constants";

export const loginUser = userData => (dispatch, getState, { AuthService }) => {
  const { email, password } = userData;
  dispatch({
    type: LOGIN
  });
  if (!validateEmail(email) || !email || !password || !password.length >= 6) {
    const error = {};

    if (!validateEmail(email) || !email) {
      error.email = "Email is incorrect";
    }

    if (!password || password.length <= 6) {
      error.password = "Password is incorrect";
    }

    dispatch({
      type: LOGIN_ERROR,
      payload: error
    });
  } else {
    AuthService.login(userData)
      .then(payload => {
        const { token } = payload.data;
        const decoded = jwt_decode(token);

        localStorage.setItem("jwtToken", token);
        setAuthToken(token);

        dispatch({
          type: LOGIN_SUCCESS,
          payload: decoded
        });
      })
      .catch(err => {
        dispatch({
          type: LOGIN_ERROR,
          payload: err.response.data
        });
      });
  }
};

export const registerUser = userData => (
  dispatch,
  getState,
  { AuthService }
) => {
  const { email, name, password, confirm_password } = userData;
  dispatch({
    type: REGISTRATION
  });

  if (
    !validateEmail(email) ||
    !email ||
    !password ||
    password.length < 6 ||
    confirm_password !== password
  ) {
    const error = {};

    if (!validateEmail(email) || !email) {
      error.email = "Email is incorrect";
    }

    if (!password || password.length < 6) {
      error.password = "Password is incorrect";
    }

    if (!name) {
      error.name = "Name is empty";
    }

    if (confirm_password !== password) {
      error.confirm_password = "Password is not equial";
    }

    dispatch({
      type: REGISTRATION_ERROR,
      payload: error
    });
  } else {
    console.log(12312312312312);
    AuthService.registation(userData)
      .then(payload => {
        dispatch({
          type: REGISTRATION_SUCCESS,
          payload
        });
      })
      .catch(err => {
        const error = err.response.data;
        dispatch({
          type: REGISTRATION_ERROR,
          payload: error
        });
      });
  }
};

export const setCurrentUser = decoded => {
  return {
    type: LOGIN_SUCCESS,
    payload: decoded
  };
};

export const logoutUser = () => dispath => {
  localStorage.removeItem("jwtToken");
  setAuthToken(false);
  dispath({
    type: LOGIN,
    payload: null
  });
};
