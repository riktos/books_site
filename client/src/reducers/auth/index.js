import {
  REGISTRATION,
  REGISTRATION_SUCCESS,
  REGISTRATION_ERROR,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR
} from "./constants";

const initialState = {
  registration: {
    isLoading: false,
    payload: null,
    error: {}
  },
  login: {
    isLoading: false,
    payload: null,
    error: {}
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case REGISTRATION:
      return {
        ...state,
        registration: { isLoading: true, payload: null, error: {} }
      };
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        registration: { isLoading: false, payload: action.payload, error: {} }
      };
    case REGISTRATION_ERROR:
      return {
        ...state,
        registration: { isLoading: false, payload: null, error: action.payload }
      };
    case LOGIN:
      return { ...state, login: { isLoading: true, payload: null, error: {} } };
    case LOGIN_SUCCESS:
      return {
        ...state,
        login: { isLoading: false, payload: action.payload, error: {} }
      };
    case LOGIN_ERROR:
      return {
        ...state,
        login: { isLoading: false, payload: null, error: action.payload }
      };
    default:
      return state;
  }
}
