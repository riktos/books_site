export const REGISTRATION = 'registration/start';
export const REGISTRATION_SUCCESS = 'registration/success';
export const REGISTRATION_ERROR = 'registration/error';

export const LOGIN = 'login/start';
export const LOGIN_SUCCESS = 'login/success';
export const LOGIN_ERROR = 'login/error';
