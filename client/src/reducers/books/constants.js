export const ALL_BOOKS = "all_books/start";
export const ALL_BOOKS_SUCCESS = "all_books/success";
export const ALL_BOOKS_ERROR = "all_books/error";

export const BOOK = "book/start";
export const BOOK_SUCCESS = "book/success";
export const BOOK_ERROR = "book/error";