import {
  ALL_BOOKS,
  ALL_BOOKS_SUCCESS,
  ALL_BOOKS_ERROR,
  BOOK,
  BOOK_ERROR,
  BOOK_SUCCESS
} from "./constants";

const initialState = {
  allBooks: {
    isLoading: false,
    payload: null,
    error: {}
  },
  bookData: {
    isLoading: false,
    payload: null,
    error: {}
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ALL_BOOKS:
      return {
        ...state,
        allBooks: { isLoading: true, payload: null, error: {} }
      };
    case ALL_BOOKS_SUCCESS:
      return {
        ...state,
        allBooks: { isLoading: false, payload: action.payload, error: {} }
      };
    case ALL_BOOKS_ERROR:
      return {
        ...state,
        allBooks: { isLoading: false, payload: null, error: action.payload }
      };
    case BOOK:
      return {
        ...state,
        bookData: { isLoading: true, payload: null, error: {} }
      };
    case BOOK_SUCCESS:
      return {
        ...state,
        bookData: { isLoading: false, payload: action.payload, error: {} }
      };
    case BOOK_ERROR:
      return {
        ...state,
        bookData: { isLoading: false, payload: null, error: action.payload }
      };
    default:
      return state;
  }
}
