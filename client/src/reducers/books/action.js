import {
  ALL_BOOKS,
  ALL_BOOKS_SUCCESS,
  ALL_BOOKS_ERROR,
  BOOK,
  BOOK_SUCCESS,
  BOOK_ERROR
} from "./constants";

export const viewAllBooks = queryParams => (
  dispatch,
  getState,
  { BooksService }
) => {
  dispatch({ type: ALL_BOOKS });

  BooksService.viewAllBooks(queryParams)
    .then(payload => {
      dispatch({
        type: ALL_BOOKS_SUCCESS,
        payload: { books: payload.data.books, items: payload.data.items }
      });
    })
    .catch(err => {
      dispatch({
        type: ALL_BOOKS_ERROR,
        payload: { books: "page not found" }
      });
    });
};

export const getBook = key => (dispatch, getState, { BooksService }) => {
  dispatch({
    type: BOOK
  });

  BooksService.getBookById(key)
    .then(res => {
      dispatch({
        type: BOOK_SUCCESS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: BOOK_ERROR,
        payload: { book: "book not found" }
      });
    });
};
