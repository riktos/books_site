import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import auth from "./auth/index";
import books from "./books/index";

export default history => combineReducers({
  router: connectRouter(history),
  auth,
  books,
});
