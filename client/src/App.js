import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import createStore from "./store";

import jwt_decode from "jwt-decode";
import { setCurrentUser } from "./reducers/auth/actions";
import { createBrowserHistory } from "history";

import Navbar from "./components/Navbar";
import Loader from "./components/Loader";
import Landing from "./pages/Landing";
import PrivateRoute from "./components/private-route/PrivateRoute";
import "./style/index.scss";

const Register = lazy(() => import("./pages/Register"));
const Login = lazy(() => import("./pages/Login"));
const AddBook = lazy(() => import("./pages/AddBook"));
const Dashboard = lazy(() => import("./pages/Dashboard"));
const BookPage = lazy(() => import("./pages/BookPage"));

const hist = createBrowserHistory();
const store = createStore(hist);

const App = () => {
  if (localStorage.jwtToken) {
    store.dispatch(setCurrentUser(jwt_decode(localStorage.jwtToken)));
  }
  return (
    <Provider store={store}>
      <Router hist={hist}>
        <Navbar />
        <Route exact path="/" component={Landing} />
        <Suspense fallback={<Loader />}>
          <Route path="/register" component={Register} />
        </Suspense>
        <Suspense fallback={<Loader />}>
          <Route path="/login" component={Login} />
        </Suspense>
        <Suspense fallback={<Loader />}>
          <Switch>
            <PrivateRoute path="/add-book" component={AddBook} />
            <PrivateRoute path="/books" component={Dashboard} />
            <PrivateRoute path="/book/:id" component={BookPage} />
          </Switch>
        </Suspense>
      </Router>
    </Provider>
  );
};

export default App;
