import React from "react";
import { Link } from "react-router-dom";
import { Card } from "antd";
import "./style.scss";

const BookCard = props => {
  return (
    <div className="card">
      <Link className="card__body" to={"/book/" + props.book._id}>
        <Card
          hoverable
          size="small"
          cover={
            <img
              alt={props.book.title}
              className="card__image"
              src={props.book.image}
            />
          }
        >
          <Card.Meta className="card__title" title={props.book.title} />
          {props.book.desc === ""
            ? "Desc is unavailable"
            : props.book.desc.slice(0, 50) + "..."}
        </Card>
      </Link>
    </div>
  );
};

export default BookCard;
