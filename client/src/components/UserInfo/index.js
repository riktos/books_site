import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { logoutUser } from "../../reducers/auth/actions";
import "./style.scss";

const UserInfo = () => {
  const dispatch = useDispatch();
  const { payload } = useSelector(state => state.auth.login);

  const OnLogoutClick = () => {
    // e.preventDefault();
    dispatch(logoutUser());
  };

  return (
    <div className="UserInfo">
      <span className="UserInfo__name">{payload.name}</span>
      <button className="UserInfo__logout" onClick={() => OnLogoutClick()}>
        Log Out
      </button>
    </div>
  );
};

export default UserInfo;
