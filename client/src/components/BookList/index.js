import React, { useState, useEffect } from "react";
import { viewAllBooks } from "../../reducers/books/action";
import { useDispatch, useSelector } from "react-redux";

import { List } from "antd";

import Loader from "../Loader";
import BookCard from "../BookCard";
import "./style.scss";

const BookList = () => {
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  useEffect(() => {
    dispatch(viewAllBooks());
  }, [dispatch]);

  const { payload, isLoading } = useSelector(state => state.books.allBooks);

  if (!payload && isLoading === false) {
    return null;
  }
  if (!payload && isLoading === true) {
    return <Loader />;
  }

  const { books, items } = payload;

  const onChangePage = page => {
    setCurrentPage(page);
    const queryParams = { params: { page: page, size: 12 } };
    dispatch(viewAllBooks(queryParams));
  };

  return (
    // <div className="BookList">
    <div className="BookList">
      {
        <List
          className="BoolList__container"
          grid={{
            gutter: 6,
            xs: 1,
            sm: 2,
            md: 2,
            lg: 3,
            xl: 6,
            xxl: 6,
          }}
          dataSource={books}
          renderItem={(book, k) => (
            <List.Item className="BoolList__item">
              <BookCard book={book} key={k} />
            </List.Item>
          )}
          pagination={{
            current: currentPage,
            onChange: page => onChangePage(page),
            pageSize: 12,
            total: items
          }}
        />
      }
    </div>
    // </div>
  );
};

export default BookList;
