import React from "react";
import { Link } from "react-router-dom"
import "./style.scss"

const SignIn = () => {
    return ( 
        <div className="SignIn">
            <Link to="/register" className="SignIn__button">Sign In</Link>
            <Link to="/login" className="SignIn__button">Sign Up</Link>
        </div>
     );
}
 
export default SignIn;
