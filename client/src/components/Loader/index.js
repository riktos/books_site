import React from 'react';
import { Spin, Icon } from 'antd'
import './style.scss';

const Loader = () => {
    const antIcon = <Icon type="loading" style={{ fontSize: 40 }} spin />;
    return (
        <div className="Loader">
            <Spin indicator={antIcon} size="large" tip="Page is loading. Please, wait..."></Spin>

        </div>
    )

}

export default Loader