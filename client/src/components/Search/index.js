import React from 'react';
import { Input } from "antd"
// import useDispatchmodule

import "./style.scss"

const Search = () => {
    return (
        <Input.Search
        className="Search"
        placeholder="Find needs book"
        enterButton
        type="flex"
        size="large"
        onSearch={value => {return value}}
      />
    )
}

export default Search;
