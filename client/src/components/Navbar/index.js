import React from "react";
import { Link } from "react-router-dom";
import {  useSelector } from 'react-redux';

import SignIn from "../SignIn";
import UserInfo from "../UserInfo"
import "./style.scss";

const Navbar = () => {
  const { payload } = useSelector(state => state.auth.login)

  return (
    <div className="Navbar">
      <Link className="Navbar__logo" to="/">
        <div className="Navbar__logo__icon">
          <i className="material-icons">book</i>
        </div>
        <div className="Navbar__logo__body">Books</div>
      </Link>
      { !payload ? <SignIn /> : <UserInfo /> }
      
    </div>
  );
};

export default Navbar;
