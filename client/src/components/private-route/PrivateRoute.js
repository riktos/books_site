import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const PrivateRoute = ({ component: Component, payload, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        payload ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
};

PrivateRoute.propTypes = {
  payload: PropTypes.any
};

const mapStateToProps = ({
  auth: {
    login: { payload }
  }
}) => ({ payload });

export default connect(mapStateToProps)(PrivateRoute);
