import axios from "axios";

export default {
  viewAllBooks(queryParams) {
    if (!queryParams) {
      return axios.get("api/books", { params: { page: 1, size: 12, title: "" } });
    } else {
      return axios.get("api/books", queryParams);
    }
  },
  searchBooks(queryParams) {
    return axios.get("api/books/search", queryParams);
  },
  getBookById(key){
    return axios.get(`http://localhost:5000/api/books/${key}`)
  }
};
