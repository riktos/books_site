import AuthService from './AuthService';
import BooksService from './BooksService'

export default {
    AuthService,
    BooksService,
}
