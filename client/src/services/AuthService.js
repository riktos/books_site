import axios from 'axios';

export default {
  registation(userData) {
    return axios.post('/api/users/register', userData);
  },
  login(userData) {
    return axios.post('/api/users/login', userData);
  },
};
