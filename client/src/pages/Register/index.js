import React, { useState, useEffect, useCallback } from "react";
import { Icon, Input, Button } from "antd";
import { Link } from "react-router-dom";
import { registerUser } from "../../reducers/auth/actions";
import { useDispatch, useSelector } from "react-redux";
import classnames from "classnames";

import "./style.scss";

const Register = props => {
  const dispatch = useDispatch();
  const { payload, error } = useSelector(state => state.auth.registration);

  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
    confirm_password: ""
  });

  useEffect(() => {
    // if (payload) {
      // props.history.push("/login");
    // }
  }, [payload, props]);

  const onChange = useCallback(
    event => {
      setFormData({
        ...formData,
        [event.target.name]: event.target.value
      });
    },
    [formData]
  );

  const onSubmit = useCallback(
    e => {
      e.preventDefault();

      const newUser = {
        ...formData
      };

      dispatch(registerUser(newUser));
    },
    [formData, dispatch]
  );

  return (
    <div className="Register">
      <div className="Register__container">
        <div className="Register__container__header">Register</div>
        <div className="Register__container__row__input">
          <Input
            placeholder="Username"
            name="name"
            onChange={onChange}
            className={classnames("", {
              invalid: error.name
            })}
            prefix={<Icon clasname="Register__icon" type="user" />}
          />
          <span className="error">{error.name}</span>
        </div>
        <div className="Register__container__row__input">
          <Input
            placeholder="Email"
            name="email"
            onChange={onChange}
            className={classnames("", {
              invalid: error.email
            })}
            prefix={<Icon clasname="Register__icon" type="mail" />}
          />
          <span className="error">{error.email}</span>
        </div>
        <div className="Register__container__row__input">
          <Input
            placeholder="Password"
            type="password"
            name="password"
            onChange={onChange}
            className={classnames("", {
              invalid: error.password
            })}
            prefix={<Icon clasname="Register__icon" type="lock" />}
          />
          <span className="error">{error.password}</span>
        </div>
        <div className="Register__container__row__input">
          <Input
            placeholder="Confirm Password"
            type="password"
            name="confirm_password"
            onChange={onChange}
            className={classnames("", {
              invalid: error.confirm_password
            })}
            prefix={<Icon clasname="Register__icon" type="lock" />}
          />
          <span className="error">{error.confirm_password}</span>
        </div>
        <div className="Register__container__row__button">
          <Button className="Register__button" onClick={onSubmit}>
            Register
          </Button>
        </div>
        <div className="Register__container__footer">
          or <Link to="/login">login now</Link>
        </div>
      </div>
    </div>
  );
};

export default Register;
