import React, { useEffect } from "react";
import { PageHeader, Typography, Divider, Tag } from "antd";
import Loader from "../../components/Loader"
import { getBook } from "../../reducers/books/action";
import { useDispatch, useSelector } from "react-redux";
import "./style.scss";

const { Title } = Typography;

const BookPage = props => {
  const dispatch = useDispatch();
  const { payload } = useSelector((state) => state.books.bookData);
  useEffect(() => {
    dispatch(getBook(props.match.params.id));
  }, [props, dispatch]);

  if (!payload) {
    return <Loader />;
  }

  return (
    <div className="Book">
      <PageHeader
        title={payload.title}
        onBack={() => props.history.goBack()}
      />
      <div className="test">
        <div className="Book__container">
          <div className="Book__meta">
            <div className="Book__meta__image">
              <img alt="" src={payload.image} />
            </div>
            <span className="price">{payload.price}</span>
          </div>
          <Typography className="Book__info">
            <div className="Book__info__header">
              <Title level={1} className="Book__info__header__title">
                {payload.title}
              </Title>
              <span className="Book__info__header__author">
                {payload.author}
              </span>
              <div className="tag-container">
                <Tag color="blue">cust1</Tag>
                <Tag color="blue">cust2</Tag>
                <Tag color="blue">cust3</Tag>
                <Tag color="blue">cust4</Tag>
                <Tag color="blue">cust5</Tag>
              </div>
              <Divider />
            </div>
            <div className="Book__info__body">{payload.desc}</div>
          </Typography>
        </div>
      </div>
    </div>
  );
};

export default BookPage;
