import React, { lazy } from "react";

import Search from "../../components/Search";
import "./style.scss";

const BookList = lazy(() => import("../../components/BookList"));

const Dasboard = () => {
  return (
    <div className="Dashboard">
      <Search />
      <BookList />
    </div>
  );
};
export default Dasboard;
