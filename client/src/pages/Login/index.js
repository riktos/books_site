import React, { useState, useEffect, useCallback } from "react";
import { Icon, Input, Button, Checkbox } from "antd";
import { Link } from "react-router-dom";
import { loginUser } from "../../reducers/auth/actions";
import { useDispatch, useSelector } from "react-redux";
import "./style.scss";
import classnames from "classnames";

const Login = props => {
  const dispatch = useDispatch();
  const { payload, error } = useSelector(state => state.auth.login);
  const [formData, setFormData] = useState({
    email: "",
    password: ""
  });

  useEffect(() => {
    if (payload) {
      props.history.push("/books");
    }
  }, [payload, error, props]);

  const onChange = useCallback(
    e => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value
      });
    },
    [formData]
  );

  const onSubmit = () => {
    const userData = {
      ...formData
    };

    dispatch(loginUser(userData));
  };

  return (
    <div className="Login">
      <div className="Login__container">
        <div className="Login__container__header">Login</div>
        <div className="Login__container__row__input">
          <Input
            placeholder="Email"
            name="email"
            onChange={onChange}
            className={classnames("", {
              invalid: error.email || error.emailnotfound
            })}
            prefix={<Icon clasname="Login__icon" type="user" />}
          />
          <span className="error">
            {error.email}
            {error.emailnotfound}
          </span>
        </div>
        <div className="Login__container__row__input">
          <Input
            placeholder="Password"
            name="password"
            type="password"
            onChange={onChange}
            className={classnames("", {
              invalid: error.password || error.passwordincorrect
            })}
            prefix={<Icon clasname="Login__icon" type="lock" />}
          />
          <span className="error">
            {error.password}
            {error.passwordincorrect}
          </span>
        </div>
        <div className="Login__container__row__checkbox">
          <Checkbox>Remember me</Checkbox>
          <Link to="/">Forgot Password</Link>
        </div>
        <div className="Login__container__row__button">
          <Button className="Login__button" onClick={onSubmit}>
            Login
          </Button>
        </div>
        <div className="Login__container__footer">
          or <Link to="/register">register now</Link>
        </div>
      </div>
    </div>
  );
};

export default Login;
