import React, { useState, useCallback } from "react";
import { Input, PageHeader, Button } from "antd";
// import "antd/dist/antd.css";
import axios from "axios";
import "./style.scss";

const AddBook = (props) => {
  const [formData, setFormData] = useState({
    title: "",
    author: "",
    desc: "",
    price: ""
  });

  const onChange = useCallback(
    e => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value
      });
    },

    [formData]
  );

  const onSubmit = useCallback(
    e => {
      e.preventDefault();

      const newBook = {
        ...formData
      };
      
      axios
        .post("/api/books/add", newBook)
        .then(res => {
          setFormData({
            title: "",
            author: "",
            desc: "",
            price: ""
          });
        })
        .catch(err => {
          console.log("Error in CreateBook!");
        });
    },[formData]
  );


  return (
    <div className="container">
      <PageHeader className="header" title="AddBook" onBack={ () => props.history.goBack() } />
      <div className="AddBook">
        <div className="AddBook__body">
          <div className="AddBook__body__input-row">
            <span>Title</span>
            <Input name="title" value={formData.title} onChange={onChange} />
          </div>
          <div className="AddBook__body__input-row">
            <span>Author</span>
            <Input name="author" value={formData.author} onChange={onChange} />
          </div>
          <div className="AddBook__body__input-row">
            <span>Price</span>
            <Input name="price" value={formData.price} onChange={onChange} />
          </div>
          <div className="AddBook__body__input-row">
            <span>Description</span>
            <Input.TextArea
              name="desc"
              value={formData.desc}
              onChange={onChange}
            />
          </div>
          <div className="AddBook__body__input-row__button">
            <Button type="primary" onClick={onSubmit}>
              Send
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddBook;
