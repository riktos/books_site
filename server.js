const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const cors = require('cors');
const PORT = 5000;
const mongoose = require('mongoose')
const db = require('./config/keys')
const passport = require("passport");
const users = require("./routes/api/users");
const books = require("./routes/api/books")


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded())

app.use(passport.initialize());

require("./config/passport")(passport)
// app.use("/api/users", users);
// app.use('/api/books', books);

app.use('/', require('./routes'));

mongoose.connect(db.mongoURI, {useUnifiedTopology: true, useNewUrlParser: true});
const connection = mongoose.connection;
connection.once('open', function() {
    console.log("MongoDB database connection established successfully");
})

app.listen(PORT, function() {
    console.log("Server is running on PORT: " + PORT);
});