const express = require("express");
const router = express.Router();

const {
  getBook,
  deleteBook,
  newBook,
  getBooks
} = require("../../controllers/books");

router.get("/", getBooks);
router.get("/:id", getBook);
router.post("/add", newBook);
router.delete("/:id", deleteBook);

module.exports = router;
