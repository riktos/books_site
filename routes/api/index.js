const apiRoutes = require('express').Router();

const books = require('./books');
const users = require('./users');

apiRoutes.use('/books', books);
apiRoutes.use('/users', users);

module.exports = apiRoutes;