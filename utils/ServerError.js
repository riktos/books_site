class ServerError extends Error {
    constructor(statusCode = 500, message, description = '') {
        super(message);
        this.statusCode = statusCode;
        this.description = description;
    }
}

module.exports = ServerError;
